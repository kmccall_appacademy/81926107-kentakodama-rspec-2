def reverser
  yield.split(' ').map { |word| word.reverse  }.join(' ')
end

def adder(default=1)
  default + yield
end

def repeater(num=1)
  num.times {yield}
end
